package server

import "time"

// User 用户
type User struct {
	ID          uint       `gorm:"primary_key" json:"id"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:"deletedAt"`
	ActivatedAt *time.Time `json:"activatedAt"`
	Name        string     `json:"name"`
	Token       string     `json:"-"`
	Phone       string     `json:"-"`
	Signature   string     `json:"signature"` //个人签名
	Status      int        `json:"status"`
}
